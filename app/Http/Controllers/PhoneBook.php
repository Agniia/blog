<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Phone;

class PhoneBook extends Controller
{
    public function showList()
    {
    	$phones = Phone::get();
    	return view('list', compact('phones'));
    }
    
    public function showListOptions()
    {	
		$phones = Phone::get();
    	return view('update', compact('phones'));
    }
    
    public function addList(Request $request)
    {
    	if(request()->has('add')) 
       	{
			 DB::table('phones')->insert([
	            'second_name' => $request->input('second_name'),
	            'first_name' =>  $request->input('first_name'),
	            'patronymic' => $request->input('patronymic'),
	            'phone_number' =>  $request->input('phone_number')
        	]);
       	}
       	$phones = Phone::get();
    	return view('list', compact('phones'));
   } 	
    
    public function updateList(Request $request)
    {	
       	if(request()->has('update')) 
       	{
       		$id = (int)$request->input('update');
       		$params = [];
       		$params['second_name'] = $request->input('second_name');
       		$params['first_name'] = $request->input('first_name');
			$params['patronymic'] = $request->input('patronymic');
			$params['phone_number'] = $request->input('phone_number');       
			foreach($params as $key => $value)
			{
				 DB::table('phones')
	            ->where('id', $id)
	            ->update(["$key" => "$value"]);
			}
		}
		else if(request()->has('delete')) 
    	{
    		$id = (int)$request->input('delete');
    		DB::table('phones')->where('id', $id)->delete();
    	}
		$phones = Phone::get();
    	return view('update', compact('phones'));
    }
}
