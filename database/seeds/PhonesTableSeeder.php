<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PhonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('phones')->insert([
            'second_name' => str_random(10),
            'first_name' => str_random(10),
            'patronymic' => str_random(10),
            'phone_number' => '8(444) 444-44-40'
        ]);
        DB::table('phones')->insert(
            ['second_name' => str_random(10),
            'first_name' => str_random(10),
            'patronymic' => str_random(10),
            'phone_number' => '8(461) 555-55-60'
        ]);
    }
}
