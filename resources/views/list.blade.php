<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Phone Book</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            form, .form-group, button[name="add"]{
                width:100%;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Записная книжка
                </div>
                 <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 col-12">
                        <a href="/update">Внести изменения</a>
                        <table class="table">
                            <tr>
                                <td>Фамилия</td>
                                <td>Имя</td>
                                <td>Отчество</td>
                                <td>Номер</td>
                            </tr>
                        @foreach ($phones as $phone)
                             <tr>
                                <td>{{ $phone->second_name }}</td>
                                <td>{{ $phone->first_name }}</td>
                                <td>{{ $phone->patronymic }}</td>
                                <td>{{ $phone->phone_number }}</td>
                            </tr>
                        @endforeach
                         </table>
                  </div><!-- col -->
                   <div class="col-md-4 col-12">
                     <form method="post" action="/list/" class="form d-flex flex-column align-items-start">
                        <div class="form-group d-flex justify-content-between"><label for="second_name">Фамилия</label>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="second_name" type="text" name="second_name" required></div>
                        <div class="form-group d-flex justify-content-between">
                        <label for="first_name">Имя</label>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="first_name" type="text" name="first_name" required></div>
                        <div class="form-group d-flex justify-content-between">
                        <label for="patronymic">Отчество</label>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="patronymic" type="text" name="patronymic" required></div>
                        <div class="form-group d-flex justify-content-between">
                        <label for="phone_number">Номер</label>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="phone_number" type="text" name="phone_number" required></div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"><button type="submit" class="btn btn-secondary" name="add">Добавить</button>
                     </form>
                   </div><!-- col -->
                </div><!-- row -->
              </div><!-- container -->
            </div><!-- content -->
        </div>
    </body>
</html>
